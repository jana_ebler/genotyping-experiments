# Comparison to gnomAD

* experiments in `` population-genotyping/ `` need to be run prior to this (to produce a lenient set)
* gnomAD callset was downloaded from: https://gnomad.broadinstitute.org/downloads/
* paths to these files must be set in config file
* running experiments: `` snakemake -s Snakefile --use-conda ``
