# Leave-one-out experiments

* leave-one-out experiments for different methods: `` snakemake -s Snakefile --use-conda ``
* leave-one-out experiment for HLA regions: `` snakemake -s Snakefile-interesting --use-conda ``

Note: Platypus, PanGenie and GraphTyper must be installed separately and corresponding paths need to be set in config files prior to running.
