# Benchmarking experiments for PanGenie

This repository contains all scripts needed to replicate the experiments presented in preprint: https://www.biorxiv.org/content/10.1101/2020.11.11.378133v1

A pipeline for constructing a pangenome graph from haplotype-resolved assemblies is available here: https://bitbucket.org/jana_ebler/vcf-merging/src/master/

PanGenie is available here: https://bitbucket.org/jana_ebler/pangenie/src/master/



## Used Software

Most tools needed to run the experiments can be installed by creating a conda environment based on the provided environment file:

`` conda env create -f environment.yml ``

However, Platypus and PanGenie need to be installed separately and corresponding paths need to be inserted into the config files before running snakemake.

## Running Experiments

Scripts used to run all experiments can be found in the subfolders. Each of the subfolders contains a README with instructions on how to run the experiments.
