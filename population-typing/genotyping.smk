configfile: "config.json"

pangenie = config['programs']['pangenie']
concordance_variant = config['programs']['concordance_variant']
panel_vcf = config['data']['panel_vcf'] 
biallelic_panel_vcf = config['data']['biallelic_panel_vcf']
sample_index = config['data']['sample_index']
trios = config['data']['trios']
results = config['data']['outname']
giab_med_svs = config['data']['giab_med']


sample_index_unrelated = config['data']['sample_index_unrelated']
sample_index_related = config['data']['sample_index_related']
read_dir_unrelated = config['data']['read_dir_unrelated']
read_dir_related = config['data']['read_dir_related']

repeats_bed = config['data']['bed']

# list of all samples to be genotyped

all_samples = [s for s in config['data']['panel_samples']]

# stores sample -> path/to/reads.fasta.gz
samples = {}

variants=['snp', 'small-deletion', 'small-insertion', 'midsize-deletion', 'midsize-insertion', 'large-deletion', 'large-insertion']

################################################################
# read sample information and paths to read data from files
################################################################

# read all samples to be considered
for line in open(sample_index, 'r'):
	if line.startswith('#'):
		continue
	fields = line.split()
	all_samples.append(line.strip())

# read 2504 unrelated samples
for line in open(sample_index_unrelated, 'r'):
	if line.startswith('study'):
		continue
	fields = line.split('\t')
	run_id = fields[3]
	sample_name = fields[2]
	if sample_name in all_samples:
		samples[sample_name] = read_dir_unrelated + sample_name + '_' + run_id + '.fasta.gz'

# read 689 unrelated samples
for line in open(sample_index_related, 'r'):
	if line.startswith('study'):
		continue
	fields = line.split('\t')
	run_id = fields[3]
	sample_name = fields[2]
	if sample_name in all_samples:
		samples[sample_name] = read_dir_related + sample_name + '_' + run_id + '.fasta.gz'

samples['NA24385'] = "../genotyping/reads/NA24385/raw/NA24385-30.fastq.gz" 

# list of all unrelated samples (parents)
children = set([])

for line in open(trios, 'r'):
	if line.startswith('FamilyID'):
		continue
	fields = line.split()
	sample = fields[1]
	if fields[2] != '0' and fields[3] != '0':
		children.add(sample)

unrelated_samples = [s for s in all_samples if not s in children]
print( str(len(unrelated_samples)) + ' unrelated samples.')

panel_samples = config['data']['panel_samples'] 

rule all:
	input:
		expand("{results}/genotyping/all-samples_{which}.vcf", results=results, which=['bi_all', 'multi_all']),
		expand("{results}/genotyping/filtered/{population}_{which}_{filter}.vcf.gz", results=results, population="all-samples", which=["bi_all"], filter=["unfiltered", "lenient", "strict"]),
		expand("{results}/evaluation/statistics/{filter}/unrelated-samples_{filter}_hwe.tsv", results=results, filter=['unfiltered','lenient', 'strict']),
		expand("{results}/evaluation/statistics/all/summary_bi_all.tsv", results=results),
		expand("{results}/evaluation/statistics/all/plot_bi_all_{vartype}_{filter}.pdf", vartype=["snps", "small_insertions", "small_deletions", "midsize_insertions", "midsize_deletions", "large_insertions", "large_deletions"], results=results, filter=['unfiltered','strict']),
		expand("{results}/evaluation/statistics/all/plot_bi_all_{vartype}_{filter}.pdf", vartype=["large_insertions", "large_deletions"], results=results, filter=['lenient_-0.5'])



################################################
# genotype all samples using PanGenie
################################################


# genotype a sample using PanGenie
rule genotyping:
	input:
		reads = lambda wildcards: samples[wildcards.sample],
		reference = config['data']['reference'],
		panel = panel_vcf
	output:
		reads = temp("{results}/genotyping/{sample}-reads.fastq"),
		paths = temp("{results}/genotyping/{sample}_path_segments.fasta"),
		genotypes = temp("{results}/genotyping/{sample}_genotyping.vcf")
	threads: 24
	log:
		"{results}/genotyping/{sample}.log"
	params:
		prefix = "{results}/genotyping/{sample}"
	resources:
		mem_total_mb=100000,
		runtime_hrs=3,
		runtime_min=59
	run:
		# jellyfish requires that the files are uncompressed
		shell("gunzip -c {input.reads} > {output.reads}")
		shell("(/usr/bin/time -v {pangenie} -i {output.reads} -v {input.panel} -r {input.reference} -o {params.prefix} -s {wildcards.sample} -j {threads} -t {threads} -g) &> {log}")


# compress (multi-allelic) vcf
rule postprocess:
	input:
		genotypes = "{results}/genotyping/{sample}_genotyping.vcf"
	output:
		output_multi_all="{results}/genotyping/{sample}_genotyping_multi_all.vcf.gz"
	threads: 1
	log:
		"{results}/genotyping/{sample}_genotyping_postprocess.log"
	resources:
		mem_total_mb=5000,
		runtime_hrs=0,
		runtime_min=20
	conda:
		"../env/genotyping.yml"
	shell:
		"(/usr/bin/time -v ./postprocess-multi.sh {input.genotypes} {output.output_multi_all} ) &> {log}"


# create bi-allelic vcf and compress it
rule create_biallelic_vcf:
	input:
		vcf="{results}/genotyping/{sample}_genotyping.vcf",
		panel=biallelic_panel_vcf
	output:
		vcf_bi_all="{results}/genotyping/{sample}_genotyping_bi_all.vcf.gz"
	log:
		"{results}/genotyping/{sample}_genotyping_bi.log"
	resources:
		mem_total_mb=20000,
		runtime_hrs=0,
		runtime_min=20
	conda:
		"../env/genotyping.yml"
	shell:
		"(/usr/bin/time -v ./postprocess-bi.sh {input.panel} {input.vcf} {output.vcf_bi_all}) &> {log}"


# merge all sample specific vcfs
rule merge_vcfs:
	input:
		lambda wildcards: expand("{{results}}/genotyping/{sample}_genotyping_{{which, bi_all|multi_all}}.vcf.gz", sample=all_samples if wildcards.population == "all-samples" else unrelated_samples)
	output:
		"{results}/genotyping/{population ,all-samples|unrelated-samples}_{which, bi_all|multi_all}.vcf"
	conda:
		'../env/genotyping.yml'
	resources:
		mem_total_mb = 150000,
		runtime_hrs = 20
	shell:
		"bcftools merge {input} > {output}"



################################################################
# HWE testing and analysis
################################################################

rule filtered_callsets:
	input:
		vcf="{results}/genotyping/{population}_bi_all.vcf",
		filters="{results}/evaluation/statistics/all/plot_bi_all_filters.tsv"
	output:
		"{results}/genotyping/filtered/{population}_bi_all_{filter, unfiltered|lenient|strict}.vcf"
	resources:
		mem_total_mb=20000,
		runtime_hrs=1,
		runtime_min=59
	shell:
		"cat {input.vcf} | python3 ../scripts/select_ids.py {input.filters} {wildcards.filter} > {output}"

rule test_hwe:
	input:
		vcf="{results}/genotyping/filtered/unrelated-samples_bi_all_{filter, unfiltered|lenient|strict}.vcf",
		bed= lambda wildcards: repeats_bed if wildcards.region == "repeat" else wildcards.results + "/bed/non-repetitve-regions.bed"
	output:
		hwe="{results}/evaluation/hwe/unrelated-samples_{filter, unfiltered|lenient|strict}_{varianttype}-{region}.hwe",
		vcf="{results}/evaluation/hwe/unrelated-samples_{filter, unfiltered|lenient|strict}_{varianttype}-{region}.vcf"
	params:
		prefix = "{results}/evaluation/hwe/unrelated-samples_{filter, unfiltered|lenient|strict}_{varianttype}-{region}",
		bedtools = lambda wildcards: "-u" if wildcards.region == "repeat" else "-f 1.0 -u" 
	conda:
		'../env/vcftools.yml'
	wildcard_constraints:
		filter="unfiltered|lenient|strict",
		region="repeat|nonrep",
		varianttype="|".join(variants)
	resources:
		mem_total_mb=20000,
		runtime_hrs=1,
		runtime_min=59
	shell:
		"""
		cat {input.vcf} | python3 ../scripts/extract-varianttype.py {wildcards.varianttype} | bedtools intersect -header -a - -b {input.bed} {params.bedtools} > {output.vcf}	
		vcftools --vcf {output.vcf} --hardy --max-missing 0.9 --out {params.prefix}
		"""



# get all regions outside the repeats in "repeats_bed"
rule get_non_repeat_regions:
	input:
		fai=config['data']['reference'] + '.fai',
		repeats=repeats_bed
	output:
		subset_bed=temp("{results}/bed/bed-tmp.bed"),
		fai=temp("{results}/bed/fai-tmp.bed"),
		bed=temp("{results}/bed/non-repetitve-regions.bed")
	conda:
		"../env/genotyping.yml"
	shell:
		"bash ../scripts/non-repetitive-regions.sh {input.repeats} {output.subset_bed} {input.fai} {output.fai} {output.bed}"

def hwe_statistics_files(wildcards):
	files = []
	for var in variants:
		for reg in ["repeat", "nonrep"]:
			files.append("{results}/evaluation/hwe/unrelated-samples_{filter}_{varianttype}-{region}.hwe".format(results=wildcards.results, filter=wildcards.filter, varianttype=var, region=reg))
	return files

def hwe_statistics_labels(wildcards):
	labels=[]
	for var in variants:
		for reg in ["repeat", "nonrep"]:
			labels.append(var + '-' + reg)
	return labels
	

rule compute_hwe_statistics:
	input:
		hwe_statistics_files
	output:
		tsv="{results}/evaluation/statistics/{filter}/unrelated-samples_{filter}_hwe.tsv"
	log:
		"{results}/evaluation/statistics/{filter}/unrelated-samples_{filter}.log"
	params:
		outname="{results}/evaluation/statistics/{filter}/unrelated-samples_{filter}",
		labels=hwe_statistics_labels
	conda:
		'../env/genotyping.yml'
	resources:
		mem_total_mb=20000,
		runtime_hrs=2
	shell:
		"python3 ../scripts/hwe.py {input} --labels {params.labels} --outname {params.outname} &> {log}"


###################################################
# compute mendelian consistency
###################################################

# count variants mendelian consistent in 0,1,2,...,nr_trios trios
rule check_consistent_trios:
	input:
		vcf="{results}/genotyping/all-samples_bi_all.vcf",
		ped=trios,
		samples=sample_index
	output:
		variant_stats="{results}/evaluation/statistics/all/mendelian-statistics_bi_all.tsv",
		trio_stats="{results}/evaluation/statistics/all/trio-statistics_bi_all.tsv"
	log:
		"{results}/evaluation/statistics/all/mendelian-statistics_bi_all.log"
	conda:
		"../env/genotyping.yml"
	resources:
		mem_total_mb=250000,
		runtime_hrs=70,
		runtime_min=1
	shell:
		"python3 ../scripts/mendelian-consistency.py statistics -vcf {input.vcf} -ped {input.ped} -samples {input.samples} -table {output.variant_stats} -column-prefix pangenie > {output.trio_stats}"


###################################################
# compute allele frequency/genotype statistics
###################################################

rule compute_statistics:
	input:
		vcf="{results}/genotyping/unrelated-samples_bi_all.vcf",
		panel=biallelic_panel_vcf
	output:
		"{results}/evaluation/statistics/all/genotyping-statistics_bi_all.tsv"
	conda:
		'../env/genotyping.yml'
	resources:
		mem_total_mb=50000,
		runtime_hrs=17,
		runtime_min=59
	shell:
		"python3 ../scripts/collect-vcf-stats.py {input.panel} {input.vcf} > {output}"



#########################################
# self-genotyping evaluation
#########################################

	
# genotyping concordance for each ID (over all samples)
rule genotype_concordance_variants:
	input:
		computed="{results}/genotyping/unrelated-samples_bi_all.vcf",
		true=biallelic_panel_vcf
	output:
		"{results}/evaluation/statistics/all/self_bi_all_variant-stats.tsv"
	params:
		file_prefix="{results}/evaluation/statistics/all/self_bi_all",
		column_prefix="pangenie_self-genotyping",
		samples=','.join(panel_samples)
	conda:
		"../env/genotyping.yml"
	resources:
		mem_total_mb=500000,
		runtime_hrs=5,
		runtime_min=1
	log:
		"{results}/evaluation/statistics/all/self_bi_all_variant-stats.log"
	shell:
		"python3 {concordance_variant} {input.true} {input.computed} {params.file_prefix} {params.samples} {params.column_prefix} &> {log}"


#################################################
# plot the results
#################################################

rule merge_table:
	input:
		"{results}/evaluation/statistics/all/genotyping-statistics_bi_all.tsv",
		"{results}/evaluation/statistics/all/mendelian-statistics_bi_all.tsv",
		"{results}/evaluation/statistics/all/self_bi_all_variant-stats.tsv"
	output:
		"{results}/evaluation/statistics/all/summary_bi_all.tsv"
	conda:
		"../env/plotting.yml"
	resources:
		mem_total_mb=50000,
		runtime_hrs=5,
		runtime_min=1
	shell:
		"python3 ../scripts/merge-tables.py {input} {output}"


rule extract_med_svs:
	input:
		med=giab_med_svs,
		biallelic=biallelic_panel_vcf,
		reference=config['data']['reference']
	output:
		 "{results}/evaluation/statistics/all/giab_svs.tsv"
	conda:
		"../env/genotyping.yml"
	shell:
		'bcftools norm -f {input.reference} -m -any {input.med} | python3 {input.biallelic} | cut -f 3 | grep -v "#" | grep -v "ID" > {output}'


rule plot_statistics:
	input:
		table="{results}/evaluation/statistics/all/summary_bi_all.tsv",
		med="{results}/evaluation/statistics/all/giab_svs.tsv"
	output:
		expand("{{results}}/evaluation/statistics/all/plot_bi_all_{vartype}_{filter}.pdf", vartype=["snps", "small_insertions", "small_deletions", "midsize_insertions", "midsize_deletions", "large_insertions", "large_deletions"], filter=['unfiltered', 'strict']),
		expand("{{results}}/evaluation/statistics/all/plot_bi_all_{vartype}_{filter}.pdf", vartype=["large_insertions", "large_deletions"], filter=['lenient_-0.5']),
		"{results}/evaluation/statistics/all/plot_bi_all_filters.tsv"
	params:
		outprefix="{results}/evaluation/statistics/all/plot_bi_all"
	log:
		"{results}/evaluation/statistics/all/plot_bi_all.log"
	conda:
		'../env/plotting.yml'
	resources:
		mem_total_mb=50000,
		runtime_hrs=5
	shell:
		"python3 ../scripts/analysis.py {input.table} {params.outprefix} {input.med} &> {log}"

rule compress:
	input:
		"{filename}.vcf"
	output:
		"{filename}.vcf.gz"
	conda:
		"../env/genotyping.yml"
	shell:
		"""
		bgzip -c {input} > {output}
		tabix -p vcf {output}
		"""
