manually downloaded this from UCSC genome browser:

https://genome.ucsc.edu/cgi-bin/hgTables?hgsid=844267153_G9YuRnBbQQmldL676jUbZWhI2VTO&clade=mammal&org=Human&db=hg38&hgta_group=rep&hgta_track=simpleRepeat&hgta_table=0&hgta_regionType=genome&position=chrX%3A15%2C560%2C138-15%2C602%2C945&hgta_outputType=bed&hgta_outFileName=

or easier: https://genome.ucsc.edu/cgi-bin/hgTables

Get to TableBrowser and select Group "Repeats" and track "Simple Repeats" 
