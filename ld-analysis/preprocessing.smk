genotypes="../population-typing/results-population-typing/genotyping/filtered/unrelated-samples_bi_all_lenient.vcf"
gwas="gwas_catalog_v1.0-associations_e100_r2021-06-08.tsv"

rule all:
	input:
		"selected-gwas-variants-present.tsv",
		"randomized-variant-ids.tsv",
		"pangenie-biallelic-svids-annotated-af-plink.bed"


rule download_gcf:
	output:
		"GCF_000001405.38.gz"
	shell:
		"wget https://ftp.ncbi.nih.gov/snp/latest_release/VCF/GCF_000001405.38.gz"

rule download_chr_map:
	output:
		"GRCh38_RefSeq2UCSC.txt"
	shell:
		"wget https://github.com/dpryan79/ChromosomeMappings/raw/master/GRCh38_RefSeq2UCSC.txt"

rule prepare_dbsnp:
	input:
		gcf="GCF_000001405.38.gz",
		map="GRCh38_RefSeq2UCSC.txt"
	output:
		"dbsnp-154-hg38.vcf.gz"
	shell:
		"zcat {input.gcf} | ./translate-chr-names.py {input.map} | bgzip > {output}"

rule index_vcf:
	input:
		"dbsnp-154-hg38.vcf.gz"
	output:
		"dbsnp-154-hg38.vcf.gz.tbi"
	conda:
		"env/gatk4.yml"
	shell:
		"bcftools index --tbi {input}"

rule prepare_genotypes:
	input:
		genotypes
	output:
		"pangenie-biallelic-svids.vcf.gz"
	conda:
		"env/gatk4.yml"
	shell:
		"cat {input} | python3 annotate.py | bgzip > {output}"

rule tabix_vcf:
	input:
		"pangenie-biallelic-svids.vcf.gz"
	output:
		"pangenie-biallelic-svids.vcf.gz.tbi"
	conda:
		"env/gatk4.yml"
	shell:
		"tabix -p vcf {input}"

rule annotate:
	input:
		pangenie= "pangenie-biallelic-svids.vcf.gz",
		pan_tbi = "pangenie-biallelic-svids.vcf.gz.tbi",
		dbsnp="dbsnp-154-hg38.vcf.gz",
		tbi="dbsnp-154-hg38.vcf.gz.tbi"
	output:
		"pangenie-biallelic-svids-annotated.vcf.gz"
	conda:
		"env/gatk4.yml"
	resources:
		runtime_hrs=5
	shell:
		"gatk  VariantAnnotator --variant {input.pangenie} --output {output} --dbsnp {input.dbsnp}"

rule add_tags:
	input:
		"pangenie-biallelic-svids-annotated.vcf.gz"
	output:
		"pangenie-biallelic-svids-annotated-af.vcf.gz"
	conda:
		"env/gatk4.yml"
	shell:
		"bcftools +fill-tags {input} -Oz -o {output} -- -t AN,AC,AF"

rule afs:
	input:
		"pangenie-biallelic-svids-annotated-af.vcf.gz"
	output:
		"afs.tsv"
	conda:
		"env/gatk4.yml"
	shell:
		"bcftools query -f '%ID\\t%AF\\n' {input} > {output}"

rule plink_pre:
	input:
		"pangenie-biallelic-svids-annotated-af.vcf.gz"
	output:
		"pangenie-biallelic-svids-annotated-af-plink.bed"
	conda:
		"env/gatk4.yml"
	params:
		"pangenie-biallelic-svids-annotated-af-plink"
	shell:
		"plink --vcf {input} --out {params}"


rule select_gwas:
	input:
		gwas=gwas,
		afs="afs.tsv"
	output:
		selected="selected-gwas-variants.tsv",
		present="selected-gwas-variants-present.tsv"
	run:
		shell("cut -f 21 {input} |sort -V |uniq -c |awk '$1>=5' | awk '$2 ~ /^rs/ {{print $2}}' |cut -f 1 -d '-'   > {output.selected}")
		shell("comm -12 <(cut -f 1 {input.afs} |sort) <(sort {output.selected}) > {output.present}")

rule randomize:
	input:
		afs="afs.tsv",
		selected="selected-gwas-variants-present.tsv"
	output:
		random="randomized-variants.tsv",
		ids="randomized-variant-ids.tsv"
	resources:
		mem_total_mb=10000,
		runtime_hrs=1
	run:
		shell("./randomize-variants.py {input.afs} {input.selected} > {output.random}")
		shell("cut -d ' ' -f 2 {output.random} > {output.ids}")
