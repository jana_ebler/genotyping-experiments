#!/usr/bin/env python3
import sys
from argparse import ArgumentParser


def main():
	parser = ArgumentParser(prog='translate-chr-names.py', description=__doc__)
	#parser.add_argument('--only-indels', default=False, action='store_true',
		#help='Only work on indels.')
	parser.add_argument('table', metavar='TABLE', help='File with translation table')
	#parser.add_argument('vcf', metavar='VCF', help='VCF files to compare')
	args = parser.parse_args()
	name_map = dict( tuple(line.split()) for line in open(args.table) )
	for line in sys.stdin:
		if line.startswith('#'):
			sys.stdout.write(line)
		else:
			fields = line.split('\t')
			if not fields[0] in name_map:
				continue
			fields[0] = name_map[fields[0]]
			sys.stdout.write('\t'.join(fields))

if __name__ == '__main__':
	main()
