import sys

chromosomes = ['chr' + str(i) for i in range(1,23)] + ['chrX']

for line in sys.stdin:
	if line.startswith('#'):
		print(line[:-1])
		continue
	fields = line.strip().split()
	if not fields[0] in chromosomes:
		continue
	info = { i.split('=')[0] : i.split('=')[1] for i in fields[7].split(';') if '=' in i }
	assert 'ID' in info
 
	# make sure VCF is biallelic
	assert len(fields[4].split(',')) == 1

	varlen = int(info['ID'].split('-')[-1])
	if varlen >= 50:
		fields[2] = info['ID']
	else:
		fields[2] = '.'
	print('\t'.join(fields))
