from string import ascii_letters
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import sys

def scale(a,b,min,max, n):
	return (((b-a)*(n-min)) / (max-min)) + a

def get_lower_tri_heatmap(df, positions, gwas, output):
    mask = np.zeros_like(df, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    # Want diagonal elements as well
    mask[np.diag_indices_from(mask)] = False

    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(11, 9))

    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(220, 10, as_cmap=True)

    # Draw the heatmap with the mask and correct aspect ratio
    sns_plot = sns.heatmap(df, mask=mask, cmap=cmap, vmax=1, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5}, yticklabels=False, xticklabels=False)

    # plot genomic positions and lines
    x1 = [i+0.5 for i in range(0,len(df))]
    y1 = [i+0.5 for i in range(0,len(df))]
    x2 = [i+20 for i in positions]
    y2 = [i for i in positions]

    plt.plot(x1, y1, color="white")
    labels = list(df.columns.values)    
    plt.plot(x2, y2, zorder=1)
    
    # connect heatmap positions and genomic positions
    index = 0
    for xy in zip(x2, y2):
        if 'INS' in labels[index] or 'DEL' in labels[index] or labels[index] in gwas:
            ax.annotate(labels[index], xy=xy, rotation=-45, fontsize=6, va='top')
        index += 1

    for i in range(len(df)):
        plt.plot([x1[i],x2[i]], [y1[i], y2[i]], 'k-', linewidth=0.5)
        if 'INS' in labels[i] or 'DEL' in labels[i] or labels[i] in gwas:
            # mark rows  
            plt.plot([0, y1[i]-0.5], [x1[i]+0.5, x1[i]+0.5], 'k-', linewidth=0.5)
            plt.plot([0, y1[i]-0.5], [x1[i]-0.5, x1[i]-0.5], 'k-', linewidth=0.5)
            # mark columns
            plt.plot([x1[i]+0.5, x1[i]+0.5], [y1[i]+0.5, len(df)], 'k-', linewidth=0.5)
            plt.plot([x1[i]-0.5, x1[i]-0.5], [y1[i]+0.5, len(df)], 'k-', linewidth=0.5)
            # annotations
            ax.annotate(labels[i], xy=(0,x1[i]+0.5), fontsize=6, ha='right')
            ax.annotate(labels[i], xy=(x1[i]-0.5,len(df)), fontsize=6, va='bottom', rotation=90)
            
    plt.gca().invert_yaxis()
    
    # save to file
    fig = sns_plot.get_figure()
    fig.tight_layout()
    fig.savefig(output)
    
    
matrix_file = sys.argv[1]
positions_file = sys.argv[2] 
gwas_file = sys.argv[3]
outfile = sys.argv[4]
var_to_pos = {}

for line in open(positions_file):
    fields = line.strip().split()
    assert len(fields) == 2
    var_to_pos[fields[1]] = int(fields[0])
    
gwas_labels = [i.strip() for i in open(gwas_file, 'r')]

df = pd.read_csv(matrix_file, sep="\t",index_col=0)
start = var_to_pos[list(df.columns.values)[0]]
end = var_to_pos[list(df.columns.values)[-1]]
a=1
b=len(df)-20
pos = [scale(a,b,start,end,var_to_pos[x]) for x in list(df.columns.values)]
get_lower_tri_heatmap(df, pos, gwas_labels, outfile)
