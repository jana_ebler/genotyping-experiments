GWAS_SNPS = [ line.strip() for line in open('selected-gwas-variants-present.tsv') ]
RANDOM_SNPS = [ line.strip() for line in open('randomized-variant-ids.tsv') ]
PLINKBASE='pangenie-biallelic-svids-annotated-af-plink'
VCF='pangenie-biallelic-svids-annotated-af.vcf.gz'


interesting_regions = ['chr9:133249905-133280876', 'chr1:65416697-65467968', 'chr4:87090985-87130985', 'chr12:20288851-20334978', 'chr12:28254365-28380028', 'chr5:115037966-115078000', 'chr16:89644435-89919709', 'chr2:232697299-232879000']

interesting_regions = ['chr9:133249905-133280876', 'chr12:28254365-28380028']

rule master:
	input:
#		expand('ld/gwas/{id}.ld.gz', id=GWAS_SNPS),
#		expand('ld/random/{id}.ld.gz', id=RANDOM_SNPS),
#		"hits-r2-0_8.tsv",
#		"hits-random-r2-0_8.tsv",
#		"hits-r2-0_8.vcf",
#		"all-stats.tsv",
#		"all-random-stats.tsv",
		expand("ld-interesting-regions/{region}/{region}.ld", region=interesting_regions),
		expand('ld-interesting-regions/{region}/{region}-matrix.tsv', region=interesting_regions),
		expand('ld-interesting-regions/{region}/{region}-positions.tsv', region=interesting_regions),
		expand('ld-interesting-regions/{region}/{region}-ld.pdf', region=interesting_regions)

#rule plink_ld:
#	output:
#		ld='ld/{dir}/{id}.ld.gz'
#	conda:
#		"env/gatk4.yml"
#	resources:
#		mem_total_mb=60000
#	shell:
#		'plink --memory 50000 --threads 1 --bfile {PLINKBASE} --r2 \'gz\' --ld-snp {wildcards.id} --ld-window-kb 1000 --ld-window-r2 0.0 --ld-window 100000 --with-freqs --out ld/{wildcards.dir}/{wildcards.id} > /dev/null 2>&1'


#rule collect_hits:
#	input:
#		expand('ld/gwas/{id}.ld.gz', id=GWAS_SNPS)
#	output:
#		"hits-r2-0_8.tsv"
#	shell:
#		"zcat ld/gwas/*.ld.gz | sed -r 's/[\t ]+/\t/g;s/^\t//g' |grep -E '(INS|DEL)' |awk '$9>=.8' > {output}"


#rule collect_random:
#	input:
#		expand('ld/random/{id}.ld.gz', id=RANDOM_SNPS)
#	output:
#		"hits-random-r2-0_8.tsv"
#	shell:
#		"zcat ld/random/*.ld.gz | sed -r 's/[\t ]+/\t/g;s/^\t//g' |grep -E '(INS|DEL)' |awk '$9>=.8' > {output}"

#rule extract_hit_ids:
#	input:
#		"hits-r2-0_8.tsv"
#	output:
#		"hits-r2-0_8-ids.txt"
#	shell:
#		"cut -f7 {input} | sort | uniq > {output}"

#rule extract_hit_vcf:
#	input:
#		vcf="pangenie-biallelic-svids-annotated-af.vcf.gz",
#		ids="hits-r2-0_8-ids.txt"
#	output:
#		"hits-r2-0_8.vcf"
#	conda:
#		"env/gatk4.yml"
#	shell:
#		"bcftools view --include ID=@{input.ids} {input.vcf} > {output}"

#rule extract_all:
#	input:
#		"hits-r2-0_8.tsv"
#	output:
#		"all-stats.tsv"
#	shell:
#		"zcat ld/gwas/*.ld.gz | sed -r 's/[\t ]+/\t/g;s/^\t//g' > {output}"

#rule extract_all_random:
#	input:
#		"hits-random-r2-0_8.tsv"
#	output:
#		"all-random-stats.tsv"
#	shell:
#		"zcat ld/random/*.ld.gz | sed -r 's/[\t ]+/\t/g;s/^\t//g' > {output}"
#

###### analysis of specific regions that contained promising hits in previous steps ######

rule extract_region:
	input:
		VCF
	output:
		"ld-interesting-regions/vcf/{region}.vcf.gz"
	conda:
		"../env/genotyping.yml"
	params:
		af = lambda wildcards: "0.3" if wildcards.region == "chr9:133249905-133280876" else "0.05"
	shell:
		"bcftools view  -r {wildcards.region} {input} --min-af {params.af} | bgzip -c > {output}"

rule tabix:
	input:
		"{filename}.vcf.gz"
	output:
		"{filename}.vcf.gz.tbi"
	shell:
		"tabix -p vcf {input}"

rule plink_pre:
	input:
		vcf="ld-interesting-regions/vcf/{region}.vcf.gz",
		tbi="ld-interesting-regions/vcf/{region}.vcf.gz.tbi"
	output:
		"ld-interesting-regions/vcf/{region}.bed"
	conda:
		"env/gatk4.yml"
	params:
		"ld-interesting-regions/vcf/{region}"
	shell:
		"plink --vcf {input.vcf} --out {params}"

rule plink_ld_region:
	input:
		"ld-interesting-regions/vcf/{region}.bed"
	output:
		ld='ld-interesting-regions/{region}/{region}.ld'
	conda:
		"env/gatk4.yml"
	params:
		"ld-interesting-regions/vcf/{region}"
	resources:
		mem_total_mb=60000
	shell:
		'plink --memory 50000 --threads 1 --bfile {params} --r2  --ld-window-kb 1000 --ld-window-r2 0.0 --ld-window 100000 --with-freqs --out ld-interesting-regions/{wildcards.region}/{wildcards.region}'

rule get_positions:
	input:
		"ld-interesting-regions/vcf/{region}.vcf.gz"
	output:
		'ld-interesting-regions/{region}/{region}-positions.tsv'
	shell:
		"zcat {input} | grep -v '#' | cut -f2,3 > {output}"

rule create_matrix:
	input:
		'ld-interesting-regions/{region}/{region}.ld'
	output:
		'ld-interesting-regions/{region}/{region}-matrix.tsv'
	shell:
		"cat {input} | python3 create-ld-matrix-2.py > {output}"

rule create_ld_plot:
	input:
		matrix = 'ld-interesting-regions/{region}/{region}-matrix.tsv',
		positions = 'ld-interesting-regions/{region}/{region}-positions.tsv',
		gwas_ids = 'selected-gwas-variants-present.tsv'
	output:
		'ld-interesting-regions/{region}/{region}-ld.pdf'
	conda:
		"env/gatk4.yml"
	shell:
		"python3 plot-ld-heatmap.py {input.matrix} {input.positions} {input.gwas_ids} {output}"
