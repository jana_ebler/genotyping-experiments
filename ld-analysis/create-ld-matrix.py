import sys
from collections import defaultdict

def make_key(id1, id2):
	if id1 < id2:
		return (id1, id2)
	else:
		return (id2, id1)

def is_sv(var_id):
	if 'INS' in var_id:
		return True
	if 'DEL' in var_id:
		return True
	return False

gwas_ids = [ line.strip() for line in open(sys.argv[1], 'r')]

region = [int(sys.argv[2].split(',')[0]), int(sys.argv[2].split(',')[1]), sys.argv[2].split(',')[2]]

id_to_pos = {}

# map [id1, id2] -> r^2
matrix = {}

for line in sys.stdin:
	if 'BP' in line:
		continue
	fields = line.split()
	var1 = fields[2]
	var2 = fields[6]
	pos1 = int(fields[1])
	pos2 = int(fields[5])
	if not (region[0] <= pos1 <= region[1]):
		continue
	if not (region[0] <= pos2 <= region[1]):
		continue
	if fields[0] != region[2]:
		continue
	assert fields[0] == fields[4]
	assert var1 in gwas_ids
	if not is_sv(var2) and not var2 in gwas_ids:
		# var is neither a disease SNP nor an SV
		continue
	key = make_key(var1,var2)
	matrix[key] = fields[8]
	id_to_pos[var1] = [fields[0], pos1]
	id_to_pos[var2] = [fields[4], pos2]
	
# print
ids_full = sorted([ [i, f[0], f[1]] for i,f in id_to_pos.items() ], key=lambda x: x[2])
ids = [i[0] for i in ids_full]

print('\t'.join(['      '] + ids))

for id1 in ids:
	line = [id1]
	for id2 in ids:
		k = make_key(id1,id2)
		if k in matrix:
			line.append(matrix[k])
		else:
			line.append("nan")
	print('\t'.join(line))
