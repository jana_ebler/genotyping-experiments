# LD-analysis

* experiments in `` population-genotyping/ `` need to be run prior to this (to produce the lenient set)
* GWAS catalog (gwas_catalog_v1.0-associations_e100_r2021-06-08.tsv) downloaded from: https://www.ebi.ac.uk/gwas/api/search/downloads/full
* run preprocessing: `` snakemake -s preprocessing.smk --use-conda ``
* run LD analysis: `` snakemake -s run-plink.smk --use-conda ``
