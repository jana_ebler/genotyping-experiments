#!/usr/bin/env python3
import sys
from argparse import ArgumentParser
from collections import defaultdict
import random

def round_af(af):
	"""Round allele frequency to the nearest 1% bin"""
	return round(float(af)*100)/100

def main():
	parser = ArgumentParser(prog='randomize-variants.py', description=__doc__)
	#parser.add_argument('--only-indels', default=False, action='store_true',
		#help='Only work on indels.')
	parser.add_argument('aftable', metavar='AFTABLE', help='Two column TSV with variant id and AF')
	parser.add_argument('variants', metavar='VARIANTS', help='One column TSV with variant ids to be randomized')
	#parser.add_argument('vcf', metavar='VCF', help='VCF files to compare')
	args = parser.parse_args()

	id2af = dict()
	af2idlist = defaultdict(list)
	for line in open(args.aftable):
		variant_id, af = line.split()
		if not variant_id.startswith('rs'):
			continue
		af = round_af(af)
		id2af[variant_id] = af
		af2idlist[af].append(variant_id)
	
	for variant_id in open(args.variants):
		variant_id = variant_id.strip()
		af = id2af[variant_id]
		if len(af2idlist[af]) <= 1:
			random_variant_id = None
		else:
			random_variant_id = variant_id
			while random_variant_id == variant_id:
				random_variant_id = random.choice(af2idlist[af])
		print(variant_id, random_variant_id, af)

if __name__ == '__main__':
	main()
